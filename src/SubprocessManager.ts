import { Subprocess } from "bun";
import ms from "ms";
import { Config, UpstreamConfig } from "./config";

interface UpstreamInstance {
  host: string;
  config: UpstreamConfig;
  subprocess: Subprocess;
  timeout?: Timer;
}

/** Manages processes of upstream servers */
export class SubprocessManager {
  readonly #upstreams = new Map<string, UpstreamInstance>();

  constructor(private readonly config: Config) {}

  /** Spawn an upstream subprocess */
  #spawnInstance(host: string, config: UpstreamConfig) {
    const subprocess = Bun.spawn({
      cmd: config.command!.split(" "),
      cwd: config.cwd || undefined,
      stdio: ["ignore", "inherit", "inherit"],
    });

    const upstream: UpstreamInstance = { host, config, subprocess };
    this.#upstreams.set(host, upstream);
    this.#resetTimeout(upstream);
  }

  /** Re-set timeout of an upstream */
  #resetTimeout(upstream: UpstreamInstance) {
    if (!upstream.config.ttl) return;
    if (upstream.timeout) clearTimeout(upstream.timeout);

    upstream.timeout = setTimeout(() => {
      upstream.subprocess.kill(9);
      this.#upstreams.delete(upstream.host);
    }, ms(upstream.config.ttl));
    upstream.timeout.unref();
  }

  /** Ensure that the upstream is ready and return it's url */
  getInstance(host: string) {
    const config = this.config.upstreams[host];
    if (!config) throw new Error("Unknown host");

    const upstream = this.#upstreams.get(host);

    if (upstream && upstream.subprocess.exitCode == null) {
      // Reset timeout on existing instance
      this.#resetTimeout(upstream);
    } else if (config.command) {
      // Or create new one if possible
      this.#spawnInstance(host, config);
    }

    return config.url;
  }
}
