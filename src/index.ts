import type { Serve } from "bun";
import YAML from "yaml";
import ms from "ms";
import { SubprocessManager } from "./SubprocessManager";
import { Config } from "./config";

const config: Config = YAML.parse(await Bun.file("./config.yml").text());
const baseRegex = config.base && new RegExp("(\\.|^)" + config.base.replace(".", "\\.") + "$");

const subprocessManager = new SubprocessManager(config);

interface WebsocketData {
  host: string;
  socket: WebSocket;
}

export default {
  websocket: {
    open(ws) {
      ws.data.socket.onclose = (ev) => ws.close(ev.code, ev.reason);
      ws.data.socket.onmessage = (ev) => ws.send(ev.data);
    },
    message(ws, message) {
      if (ws.data.socket.readyState === ws.data.socket.OPEN) {
        ws.data.socket.send(message);
      }
    },
    close(ws, code, reason) {
      ws.data.socket.close(code, reason);
    },
  },

  async fetch(req, server) {
    const url = new URL(req.url);
    let host = url.hostname;

    // Remove the base
    if (baseRegex) host = host.replace(baseRegex, "") || "default";

    const upstreamUrl = new URL(subprocessManager.getInstance(host));

    // Generate url of upstream
    const reqUrl = new URL(url);
    reqUrl.host = upstreamUrl.host;
    reqUrl.protocol = upstreamUrl.protocol;
    reqUrl.pathname = (upstreamUrl.pathname + url.pathname).replace(/\/+/g, "/");

    // Create headers for upstream
    const reqHeaders = new Headers(req.headers);
    reqHeaders.delete("Host");
    reqHeaders.delete("Accept-Encoding");

    // Upgrade the request to a websocket connection if needed
    if (req.headers.get("upgrade")?.toLowerCase() == "websocket") {
      const socket = new WebSocket(reqUrl, {
        headers: reqHeaders,
      });

      const data: WebsocketData = { host, socket };
      const upgraded = server.upgrade(req, { data });

      if (!upgraded) {
        socket.close();
        return new Response("Upgrade failed", { status: 400 });
      }
    }
    // Otherwise continue in HTTP
    else {
      const waitStart = performance.now();

      while (true) {
        try {
          // Send request to upstream
          const res = await fetch(reqUrl, {
            method: req.method,
            headers: reqHeaders,
            body: await req.arrayBuffer(), // TODO: streaming not yet supported in Bun
            redirect: "follow",
            keepalive: true,
            // verbose: true,
          });

          // Stream back the response
          return new Response(res.body, {
            headers: res.headers,
            status: res.status,
          });
        } catch (err) {
          if (
            performance.now() - waitStart < ms(config.maxWait) &&
            err instanceof Error &&
            err.name == "ConnectionRefused"
          ) {
            continue; // Retry the connection
          }
          throw err;
        }
      }
    }
  },
  error(error) {
    if (error instanceof Error && error.message == "Unknown host") {
      return new Response("404 Not found", { status: 404 });
    }
    console.error(error);
    return new Response(String(error), {
      status: 500,
    });
  },
} satisfies Serve<WebsocketData>;
