export interface Config {
  base?: string;
  maxWait: string;
  upstreams: Record<string, UpstreamConfig>;
}

export interface UpstreamConfig {
  command?: string;
  cwd?: string;
  url: string;
  ttl: string;
}
